import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

const App = () => {
  return (
    <div className= "ui container comments">
      <ApprovalCard>
        <CommentDetail
          author="Sam"
          timeAgo="Today at 4:45 PM"
          comment_text="Great Post !!!"
          comment_img={faker.image.avatar()}
        />
      </ApprovalCard>

      <ApprovalCard>
        <CommentDetail
          author="Alex"
          timeAgo="Today at 2:45 PM"
          comment_text="Fantastic Post !!!"
          comment_img={faker.image.avatar()}
        />
      </ApprovalCard>
      
      <ApprovalCard>
        <CommentDetail
          author="Martin"
          timeAgo="Today at 3:45 PM"
          comment_text="Great Post This Is !!!"
          comment_img={faker.image.avatar()}
        />
      </ApprovalCard>      
    </div>
  );
};  

ReactDOM.render(<App />, document.querySelector('#root'));