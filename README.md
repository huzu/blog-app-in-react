## A Blog Post App built in React !!!

Some notes to remembers covered in this project:
1. We use props to pass any dynamic values from one component to another, same as we do in ruby/java etc where they are called as functions. 
2. Whenever we pass a react component as an argument to a parent react component, we need to access it inside the parent component using `props.children` as it will render the whole component and will display the same on UI. 
3. Browser don't understand the JSX code, hence Babel is used to convert this JSX into normal JS. Another point to note here is that the use of ES2015 JS and ES2016 JS and so on. Now browsers might not be able to understand these versions of JS and can end being a failure for the users and nothing will get executed on the end users browser. To solve this issue, we use BABEL which translates this ES2016 JS to normal ES5 JS file. We can see the conversion at bablejs.io
4. What babel does is converts the JSX into normal JS code which then gets executed on the browser. For example it gets converted to React.createElement(logic_here).
5. Talking about components, there are 3 principles to remember about it which are: Component Nesting, Component Reusability and Component Configuration. 
6. When we pass a component inside a component, it is available as props.children in the parent component. 

Imp Defination: 
Component: A component is a function OR a class that produces HTML to show the user(i.e. Using JSX) and handles feedback from user(i.e. Using event handlers). 
